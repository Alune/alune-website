$(document).ready(function(){
  $('.menu-toggler').click(function(){
    $('.mobile-menu').addClass('menu-opened');
  });

  $('.close-mobile-menu').click(function(){
    $('.mobile-menu').removeClass('menu-opened');
  });

  $('.logbook-cta').click(function(){
    var clientHeight = $(window).height();
    
    $("html, body").animate({ scrollTop: clientHeight + 100 }, 400);
  });
});
