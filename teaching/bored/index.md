---
layout: documentation
id: bored
---

# Bored?

*à voir / écouter / jouer / lire absolument  

#### Séries animées  
Steven Universe   
Gravity falls  
Cowboy Bebop*  
Rick and Morty*  

#### Films d'animation  
Les Miyazaki*   
Les Jim Henson  
Les Studio Aardman  
Les Pixar  
Les Satoshi Kon*  
Le Roi et l'Oiseau*  
Les Maitres du Temps*  
Akira* (film et manga)  
Ghost in the Shell* (film, série et manga)  
Who Framed Roger Rabbit  
[The thief and the cobbler](https://www.youtube.com/watch?v=yZibUpH-AME) (films jamais terminé de Richard Williams)   
The Lego Movie*  
Storks  
Fantastic Mr Fox   
Isle of Dogs  
Valse avec Bachir  
Le Congrès  
Spiderman into the spider verse  
Kubo  
Your name  
Summer Wars  
The girl who leaped through time  



#### Films
Les films de Kubrick*  
Les films de Denis Villeneuve*  
Les films de Damien Chazelle  
Les films des Frères Cohen  
Les films de Tarantino  
Star Wars  
The Silence of the Lambs - trilogy  
Hitchcock : Psycho, Vertigo, Rear Window, North by Northwest  
Sergio Leone : 3 Once apon a time movies, Good bad Ugly - trilogy  
Alien*  
Blade Runner*  
Citizen Kane  
The wizard of Oz  
One Flew Over the Cuckoo's Nest  
Singin in the rain  
Appocalypse Now  
The Deer Hunter*  
Gladiator  
Indiana Jones  
Jaws  
Duel  
Christine  
The Thing  
They Live  
Vanishing Point*  
The Graduate  
Her  
Yorgos Lanthimos : The Lobster*, The favorite, Dogtooth  
Scott Pilgrim  



#### Séries
Twin Peaks*  
The Wire*  
Homecoming  
The End Of the Fucking World  
Chernobyl  
The Soprano  
Black Mirror  
Oz  
True Detective - Première Saison*  
Silicon Valley  
The Office - US Version  
The Prisonner

#### Musique  
Fout Tet*  
Oneothrix Point Never*  
Blanck Mass  
Rival Consoles  
Mondkopf*  
Drexciya, Arpanet, Translusion etc.  
Skee Mask / SCNTST  
Aphex Twin*  
Burial*  
Boards Of Canada  
Young Echo  
Floating Points  
Caterina Barbieri  
Para One  
Ken Ishii  
Lorenzo Senni  
Simian Mobile Disco  
The Postal Service  
DJ Paypal  
Joy Division*  
New Order  
Jamie XX  
Darkside*  
Apparat / Moderat  

Tyler the Creator*  
Rosalia  
Noname  
Cannibal Ox*  
Mac Miller  
Saba  
Kano  
Goldlink  
Buddy  
Freddie Gibbs & Madlib, MF DOOM, Madvillain  
FKA Twigs  
Childish Gambino  
Obviously Kendrick, Kanye and Drake  

Kobo  
TTC  
Alpha Wann  
Kekra  
Shay  
Nekfeu  
113  
Oxmo  

  
Terry Riley   
Steve Reich*   
Moondog  


#### Jeux Video  
The Witness* - Direction artistique, expérience utilisateur  
Breath of the Wild* - Sound Design, expérience utilisateur  
Cuphead* - Direction artistique, technique  
Astroneer - Sound Design, direction artistique  
Half Life - Sound Design, Story Telling  
Hyper Light Drifter - Direction artistique, animations  
The Stanley Parable* - Story Telling  
NaissancEe - Direction Artistique  
Fez - Concept, Design

#### Livres    
Dune*  
Fondation*  
Rêve de Gloire  
La Horde de Contrevent  
La Fille Automate   
Ubik*  
Silo  

