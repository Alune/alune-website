---
layout: documentation
id: sujets
---

# Sujets
##### Motion design première année graphisme LISAA
Corentin Moussard  



<!-- <p class="text-small grey-text no-margin">Sujet 1</p> -->


###### Sujet 1
#### Found Footage - Introduction AE / Montage
Récupérer des extraits existants de films, videos et photos et réaliser un court avec. 
À la manière de *La Classe Américaine* de Michel Hazanavicius ou les différentes émissions diffusés sur Canal+ dans les années 90 comme *Messages a charactère informatif* de Nicolas Charlet et Bruno Lavaine ou certains sketch des nuls, prémices de l'humour internet et de la culture meme. Ajouter de typographie, des dessins, composer le cadre etc. Le résultat peut raconter une histoire, être une expérimental, une recherche ésthetique, ce qui vous parle. (Koulechov)   

**Rendu :** Vidéo d'environ 40 secondes.

**Techniques abordées**   
Prise en main de l'interface d'after effects, les différents effets, types de calques, les exports via AE et Media Encoder. Familiarisation avec les formats, codecs, et la majorité des contraintes que l'on rencontre en travaillant avec de la video.

**Emploie du temps**  
Du 25 septembre au 9 octobre - 3 semaines.

Max cooper > https://www.youtube.com/watch?v=nO9aot9RgQc


###### Sujet 2
#### Set d'icones animées
Conception d'un set de huit icones, cohérentes visuellement et animer chacune d'elles. Chaque animation étant très courte (2 à 3 secondes) l'idée est de focaliser les efforts sur la simplicité et la qualité des animations, et l'émotion qu'elles transmettent. [*SPAN icons*](https://vimeo.com/144987964) from J-Scott,    

**Rendu :** Vidéo, d'environ 30 secondes : mini showreel présentant les icones.

**Techniques abordées**   
Bases de l'animation et des vitesses dans after effects. Animation des formes prédéfinies dans after effects, masks (nostromo animation)  

**Emploie du temps**  
Du 16 octobre au 6 novembre - 3 semaines



###### Sujet 3
#### Animation image par image
Animation papier, photoshop ou en stopmotion sur le thème de la transformation / métamorphose. Principes d'animation, [*The Illusion of Life*](https://vimeo.com/93206523), pères de l'animation chez Disney, Richard Williams… *For Izzy* Animations par Natalia Serebrennikova.

**Rendu :** Vidéo d'environ 20 secondes.

**Techniques abordées**   
Story board, onion skining, prévision d'une animation avec les "extreme positions" et "passing positions", racourcis, exagération de mouvements.  

**Emploie du temps**  
Du 13 au 27 novembre - 3 semaines



###### Sujet 4
#### Tribute to Yule log
Animation d'une boucle sur le thème de la buche de noël, à la manière de l'événement [*Yule Log*](https://en.wikipedia.org/wiki/Yule_Log_(TV_program)) et sa [version animée](https://www.watchyulelog.com/) qui à eu lieu entre 2013 et 2015.

**Rendu :** Vidéo d'environ 20 secondes ou la première et dernière image sont identiques.

**Techniques abordées**   
Mettre l'accent sur le sound design, introduction aux expressions dans AE, plug-ins.

**Emploie du temps**  
Du 4 au 18 décembre - 3 semaines



###### Sujet 5
#### Paysage
Illustration d'un paysage et animation de celui-ci dans un environement 3D.  

**Rendu :** Vidéo d'environ 30 secondes.  

**Techniques abordées**   
Animation 3D dans AE, effet de parallax, utilisation des caméras et objets nuls, lumières.

**Emploie du temps**  
Du 15 au 29 janvier - 3 semaines



###### Sujet 6
#### Animation typographique
Dessin d'un caractère (26 glyphs, une seule casse chiffres en option) et animation d'un spécimen de cette police. Mettre l'accent sur le style de la police de caractère et créer une animation qui reflète ce style, à la manière des fontes [Animography](https://animography.net/collections/typefaces). Générique de Chronicle, Mindhunter, Alien etc.

**Rendu :** Vidéo d'environ 45 secondes.  

**Techniques abordées**   
Morphing vectoriel, précomposition et intégration d'expressions pour les rendre réutilisables facilement.

**Emploie du temps**  
Du 5 février au 5 mars - 3 semaines

Max Cooper - Perpetual Motion font at the bighining



###### Sujet 7
#### Logo, charte graphique, interface
> Peut être splitté en deux sujets  

Création d'un logo et d'une charte graphique (typographies, couleurs) et création d'une vidéo web publicitaire. L'entreprise doit proposer un produit numérique comme son offre principale ou cela doit faire partie de son offre. Designer et animer un enchainement de trois écrans de cette application.
La vidéo comprend le logo animé, une interface animée, et les grands principes d'animations > typo, transitions, concepts graphiques. Exemples d'identitées : [Culture Laval](https://vimeo.com/339609693), [Google brand system](https://vimeo.com/138093355), [Format](https://vimeo.com/214839485), [Awwwards](https://vimeo.com/269014400), [Blend Manifesto](https://vimeo.com/132101287). Exemples d'interfaces : [Muzli weekly](https://medium.muz.li/search?q=interactions).

**Rendu :** Vidéo d'environ 60 secondes.  

**Techniques abordées**   
À définir

**Emploie du temps**  
Du 11 mars au 1er avril - 4 semaines



###### Sujet 8
#### Trailer d'un festival
Réaliser un trailer pour un festival réunissant une multitude d'intervenants appartenants a un domaine comme la musique, le cinéma, l'animation etc. Le Festival adopte une identité visuelle a définir, qui refletera le secteur choisit et le thème de cette édition du festival. Le nom de chaque intervant doit figurer durant la video ainsi que quelques information sur le déroulé du festival comme le lieu, la date, le thème et le numéro d'édition du festival. Exemples : [AICP](https://vimeo.com/341335282), [KIKK](https://vimeo.com/141387499), [FITC](https://vimeo.com/118919656), [Inrocks Festival](https://vimeo.com/184849709), [Motion Motion](https://vimeo.com/265523174).
Videos sans crédits mais qui valent le coup d'oeil : [Develop Denver](https://vimeo.com/229056408), [AMP awards](https://vimeo.com/218830007).

**Rendu :** Vidéo d'environ 60 secondes.

**Techniques abordées**   
À définir

**Emploie du temps**  
Du 22 avril au 13 mai - 4 semaines