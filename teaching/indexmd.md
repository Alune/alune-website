---
layout: documentation
id: sujets
---

# Sujets
##### Motion design - LISAA G1C
Corentin Moussard  



<!-- <p class="text-small grey-text no-margin">Sujet 1</p> -->

!(ae-cheatsheet.png)[{{site.baseurl}}/assets/images/ae-cheatsheet.png]

###### Sujet 1
#### Found Footage - Introduction Ae / Montage
Récupérez des extraits existants de films, videos, photos et sons dans le but de vous les réapproprier. À partir de ceux-ci vous devez monter une video de 40 secondes ou plus, dont vous déterminerez le ton ainsi que le sujet. Le résultat peut raconter une histoire, être une vidéo expérimentale, une recherche esthétique, une suite de blagues… Votre choix sera le bon. N'hésitez pas à ajouter des visuels, de la typographie, couper et composer vos cadres, faire des doublages, utiliser des videos que vous avez faites etc.

**Références**   
* L'effet Koulechov
* Le cinéma de Méliès
* Des films :  
  - Shining (Kubrick en général)
  - Alien (et les premiers Ridley Scott)
  - Inception (Nolan en général, mention spécial à Memento)
  - 500 days of summer
  - Whiplash (Chazelle en général)
  - The Revenant & Birdman
  - You Were Never Really Here & Taxi Driver
  - Arrival (Villeneuve en général)
  - Mr. Nobody
  - Citizen Kane
* Les videos de Karim Debbache > Crossed & Chroma
* La Classe Américaine
* Messages a charactère informatif
* Memes en tout genres

**Rendu**   
Une vidéo d'environ 40 secondes, une description par écrit de votre projet en quelques lignes et la liste des extraits que vous avez utilisé.

**Techniques abordées**   
Prise en main de l'interface d'after effects, les différents effets, types de calques, les exports via Ae et Media Encoder. 
Prise en main rapide de Photoshop et Illustrator, dans le but de faire des fichiers utilisables dans Ae.
Familiarisation avec les formats, codecs, et la majorité des contraintes que l'on rencontre en travaillant avec de la video. 
Bases de l'animation dans after effects.

**Emploie du temps**  
Distribution de 25 septembre  
Suivi les 2 et 9 octobre  
Rendu le 16 octobre


