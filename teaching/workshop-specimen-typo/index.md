---
layout: docs-generic
is-child-page: false
---

# Workshop édition augmenté : spécimen typographique

#### Contexte
Lors de la sortie d'une nouvelle police de caractère, il est coutume de réaliser un ou plusieurs supports de communication, visant à promouvoir et mettre en lumière les détails de celle-ci. Ces supports sont appelés des spécimens.   
Lors de ce workshop vous produirez un spécimen pour une police de caractère de votre choix, comprenant autant de graisses (thin, medium, bold etc.) et de styles (italic, extended, narrow etc.) qu'il vous plaira.
Vous développerez un univers graphique en accord avec la typographie, et produirez des visuels afin d'habiller votre spécimen.

Votre rendu comportera deux productions : 
 - un objet éditorial, pouvant regrouper plusieurs formats.
 - un objet numérique de votre choix : une/des page(s) web, une/des expérience(s) en réalité augmentée, une/des vidéo(s) etc.
Ces supports comprendront une interface pour passer de l'un à l'autre : lien, qr code, détail du fonctionnement réalité augmentée etc.

Au moins un de vos objet devra comprendre :
 - Le titre de la police
 - Le nom de la fonderie
 - L'année de production
 - La liste des glyphs de votre police de caractère, exhaustive ou non.  
 Mis à part cela, le contenu de vos productions est libre, il peut être uniquement constitué d'expérimentations graphiques, comporter du texte (récupéré ou produit) ou être un mix des deux.

**Références**  

- Imprimées  
  - [La perruque](http://la-perruque.org/)
  - [Alexandrine Thore](http://alexandrine.thore.free.fr/specimensTypo.php)
  - [Morion](https://www.behance.net/gallery/47632005/Morion-Typeface-Specimen)
  - [LuxTypo](https://luxtypo.com/collections/goods)
  - [Ahn Sang Soo and AG Typography Institute](https://letterformarchive.org/news/view/from-the-collection-ahn-sang-soo)
  - [Tim Ruxton](https://www.underconsideration.com/fpo/archives/2013/02/tim-ruxton-self-promo-type-specimen-poster.php)
- Numériques  
  - [IBM Plex](https://www.ibm.com/plex/)
  - [Micro typeface](https://www.julienlelievre.com/projets/gemeli-micro-type-specimen/)
  - [Scope typeface](https://scope-typeface.com/)
  - [GT Flexa](https://www.gt-flexa.com/)
  - [Faune](http://www.cnap.graphismeenfrance.fr/faune/)
  - [Animography](https://animography.net/collections/typefaces)
  - [Christoph Niemann’s ar cover](https://www.newyorker.com/video/watch/introducing-christoph-niemann-augmented-reality-covers)
  - [DIA](https://www.instagram.com/dia_studio/)
  - [Type terms](https://www.supremo.co.uk/typeterms/)
- Fonderies  
  - [Grilli type](https://www.grillitype.com/)
  - [Velvetyne](https://velvetyne.fr/)
  - [Production Type](https://www.productiontype.com/)
  - [Use and Modify](https://usemodify.com/)
  - [The Designers Foundry](https://thedesignersfoundry.com/)

#### Attendus pédagogiques
- Développer un concept graphique déclinable, cohérent avec une police de caractère
- Imaginer une intéraction physique / numérique

#### Livrables
- Un objet éditorial imprimé
- Le(s) pdf de l'objet imprimé
- L'objet numérique ou à défault, une maquette de celui-ci
- Une mise en situation de l'intéraction entre les parties physique et numérique de votre projet accompagnée d'un texte décrivant celle-ci

#### Critères d'évaluation
- Direction artistique du projet
- Originalité de chacun des rendus et de leur interaction
- Propreté, rigueur et application apportée au projet
- Technique utilisée pour l'impression et la production numérique

#### Calendrier
Point d'avancement avec chaque équipe à la fin de chaque demi journée.  

- Lundi  
Matin : Présentation du workshop et des enjeux de l'interaction physique numérique.  
Après-midi : Établir les premières pistes graphiques, idées sur le format physique et sur le medium numérique.   

- Mardi   
Matin : Réalisation d'un chemin de fer de l'objet imprimé.  
Après-midi : Expérimentations du médium numérique, choix définitif du style graphique.  

- Mercredi  
Matin : Production d'une première maquette avec un ou plusieurs visuels associés, afin d'obtenir un rendu graphique conséquent sur une partie restreinte du projet.  
Après-midi : Choix définitif et début de la production du contenu numérique.  

- Jeudi  
Production des rendus finaux.  

- Vendredi  
Matin : Finalisation des projets.  
Après-midi : Rendu et présentation des projets.  
