---
layout: documentation
id: resources
---

# Resources

#### Plug-ins
*Red Giant Trap Code* - Suite de plugins d'animation  
*Duick* - Suite de plugins d'animation  
*RubberHose* - Animation de personnage, rig  
*Reposition Anchor Point* - Tout est dans le titre  
*Lottie* - Exporter comps after effects en web (canvas, svg, webgl)  

#### Tutoriels
*Mattrunks* - Motion
*Video Copilot* - 3D motion  
*Grey Scale Gorilla* - 3D motion  
*Ben Mariott* - Motion
*School of motion*  
*Motion design school*
*SuperHi* - Web  

#### Resources graphiques
*Typewolf* - blog sur la typographie   
*Use and Modify* - Référencement de typographies open-source    
*GetTheFont* - Moteur de recherche de fichiers de fontes sur GitHub   
*WhatTheFont* - Reconaissance de fontes à partir d'images  
*Fontanello* - Extension Chrome fontes d'un site  
*Awwwards* - Selection de sites internets  
*Motionographer*

#### Resources Web
*Visual Studio Code* - Editeur de code  
*Jekyll* - Generateur de sites statiques sans javascript    
*Gatsby* - Générateur de sites statiques basé sur React js   
*GitHub, GitLab* - Outil de versioning et de partage de code   
*Netlify* - Service d'hébergement / build s'intégrant à Git   
*Hyper* - terminal app en js  
*iTerm2* - terminal app  
*Oh My Zsh* - Version de terminal  


