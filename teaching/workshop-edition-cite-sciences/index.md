---
layout: docs-generic
is-child-page: false
---

# Workshop édition

#### Commanditaire
La Cité des Sciences et de l'Industrie est un espace d'exposition, qui organises des événements pour tous les ages. Au cœur de ceux-ci, une volonté de rendre la connaissance accessible à tous de manière ludique et compréhensible, sur des sujets variés.  
Le pôle communication souhaite faire évoluer les supports de présentation des expositions, afin de refléter cette démarche. Ils font appel a vous pour produire des objets de communication imprimées, qui donne un accès à du contenu additionnel numérique, permettant d'augmenter l'information de manière ludique et innovante.

#### Sujet

Cette semaine de workshop est dédiée à la conception d'un objet éditorial, ayant pour but de promouvoir une exposition ayant lieu à la Cité des Sciences et de l'Industrie. Vous travaillerez en groupes de deux ou trois, que vous constiturez au préalable. Chaque groupe se verra attribué une des expositions, le contenu lié à chacune d'elles est disponible ci-dessous.

Vous choisirez une forme d'objet, contenant un ou plusieurs formats, sur lequel vous composerez le contenu et vous l'habillerez d'une identité visuelle que vous établirez. Celle-ci comprend les typographie et leurs principes d'utilisation, les formes et couleurs, ainsi que le contenu visuel ou l'absence de contenu visuel comme de la photographie ou des illustrations.
Vous devez en plus prévoir dans votre objet les informations suivantes : la date de l'exposition, les horaires d'ouvertures, les tarifs, les partenaires, les langues ainsi que l'adresse et les informations d'accessibilité au lieu.

En plus du medium physique, votre projet comprendra une composante numérique, avec du contenu pensé pour fonctionner avec le medium choisi : réalité augmenté, page web, vidéos, articles. Vous devez prévoir une mise en situation de l'intéraction entre les parties physique et numérique de votre projet, accompagnée d'un texte décrivant celle-ci. Vous pouvez imaginer des choses qui ne sont pas à votre portée de production, mais elles doivent être réalisables.

##### Détail des livrables
- Un pdf de l'objet imprimé et un mockup représentatif de sa forme physique, l'objet en lui même sera a rendre une fois l'impression possible.
- Une expérience numérique associée à votre objet, les formes pouvant être largement variables, vous fournirez un fichier ou une url de l'expérience, ainsi qu'un description de celle-ci et de son intéraction avec la partie physique de votre projet.

#### Expositions

- Froid : [contenu](froid), [site de l'exposition](http://www.cite-sciences.fr/fr/au-programme/expos-temporaires/froid/lexposition/)
- Feu : [contenu](feu), [site de l'exposition](http://www.cite-sciences.fr/fr/au-programme/expos-temporaires/feu/)
- Cabanes : [contenu](cabanes), [site de l'exposition](http://www.cite-sciences.fr/fr/au-programme/expos-temporaires/cabanes/)


#### Références

###### Imprimées
- [Actes Nord éditions](https://www.actesnord.be/)
- [Stolen Books](http://ideiascompeso.pt/stolenbooks/)
- [Rotolux Press](http://www.rotoluxpress.com/)
- [Acedie 58](http://acedie58.fr/)
- [Incomplete Inventory](https://www.ill-studio.com/projects/incomplete-inventory)
- [Robynn Redgrave](https://www.behance.net/hellorobynne)
- [Laura Voet - Self promotion](https://www.behance.net/gallery/41331995/Creative-CV-20-Self-Promotion)
- [Christin Berger](https://www.behance.net/christinberger)

###### Numériques
- [Christoph Niemann’s ar cover](https://www.newyorker.com/video/watch/introducing-christoph-niemann-augmented-reality-covers)
- [Maison Tangible](https://www.maison-tangible.fr/)
- [Raphaël Bastide](https://raphaelbastide.com/)
- [Copy this book](https://copy-this-book.eu/)
- [Éditions Maison Maison](https://www.facebook.com/editionsmaisonmaison/)
- [Brutalist Websites](https://brutalistwebsites.com/)
- [Rafaël Rozendaal](https://www.newrafael.com/websites/)


#### Planning

Point d'avancement avec chaque équipe à la fin de chaque demi journée.  

- Lundi  
Matin : Présentation du workshop et des enjeux de l'interaction physique numérique.  
Après-midi : Établir les premières pistes graphiques, idées sur le format physique et sur le medium numérique.   

- Mardi   
Matin : Réalisation d'un chemin de fer de l'objet imprimé.  
Après-midi : Expérimentations du médium numérique, choix définitif du style graphique.  

- Mercredi  
Matin : Production d'une première maquette avec un ou plusieurs visuels associés, afin d'obtenir un rendu graphique conséquent sur une partie restreinte du projet.  
Après-midi : Choix définitif et début de la production du contenu numérique.  

- Jeudi  
Production des rendus finaux.  

- Vendredi  
Matin : Finalisation des projets.  
Après-midi : Rendu, présentation de leur projet par chacun des groupes.  


#### Critères d'évaluation

- Direction artistique du projet et son rapport avec le thème de l'exposition.
- Originalité de chacun des rendus et de leur interaction.
- Propreté, rigueur et application apportée au projet.
- Technique utilisée pour l'impression et la production numérique.