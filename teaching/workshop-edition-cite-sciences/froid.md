---
layout: docs-generic
is-child-page: true
root-folder: workshop-edition
---

# Froid
##### Contenu de l'objet éditorial à produire

Dans l’imaginaire collectif, le froid a été associé pendant longtemps à une manifestation de la nature que l’on affronte pendant les mois d’hiver.
Pour autant, ce mot n’évoque pas uniquement le froid naturel, mais également le "froid fabriqué", produit par des machines conçues par l’être humain.

Qu’est-ce que le froid ? Quels sont ses effets sur les organismes vivants ? Comment peut-on le fabriquer ? Pourquoi a-t-il révolutionné notre quotidien ? Quelles sont ses utilisations potentielles ? Comment se comporte la matière quand la température baisse ? Quels sont les projets de recherche ou les applications industrielles qui n’auraient pas pu voir le jour sans le froid ? Le froid a-t-il une limite ?

Véritable plongée dans l’univers du froid, cette exposition propose des repères concrets et donne les clés pour appréhender les phénomènes et les applications qui se cachent derrière la thématique.

- Des températures repères  
À l’entrée de l’exposition, une grande allée sensorielle enveloppante inspirée par la métaphore du thermomètre permet de se forger des repères et de situer les divers phénomènes, objets, pratiques ou applications liés à différents "froids".
Cette échelle de température marque les principales étapes de la descente de 37 °C, la température interne de notre corps, jusqu’à -273,15 °C, le zéro absolu qui est la limite ultime du froid.
Le long de cette allée, trois ouvertures mènent vers les trois parties thématiques de l’exposition, les "défis".

- Le froid : un défi pour l'humanité  
Découvrez les défis relevés grâce au froid en parcourant trois espaces correspondant à des univers différents : corps humain et organismes vivants, vie quotidienne et applications industrielles, science et recherche.

#### Défis pour le vivant

Découvrez ce qui se cache sous la notion de "froid ressenti", pour les humains, les animaux ou les plantes.

Face au froid, comment réagissent les êtres vivants ? Quels sont les dégâts pour notre organisme ? Les effets sur les plantes ? Comment certains animaux peuvent-ils survivre là où le froid est le plus extrême ? Comment utiliser le froid pour guérir, conserver ou étudier le vivant ? Peut-on cryoconserver un humain ? Le froid permet-il au vivant de repousser certaines limites ? Lesquelles ?  

Vivre avec le froid :  
L’eau étant le principal constituant des organismes vivants, leurs cellules risquent d’être endommagées par la formation de cristaux de glace dès lors que la température descend au-dessous de 0 °C. Cependant, certains organismes très particuliers semblent échapper à cette fatalité.
Dans l'espace "Défis pour le vivant", composé de trois îlots thématiques, découvrez les limites que le froid permet de repousser lorsque l’on se réfère au vivant :

- Humains face au froid
- Animaux et plantes : composer avec le froid
- Guérir grâce au froid ?

###### À ne pas manquer

Regarde-toi quand tu as froid ! : entrez dans une cabine où la température est de l'ordre de 5°C et découvrez les mécanismes déclenchés dans notre corps humain.

L’endurcissement au gel : suivez l'évolution d'un bourgeon au fil de l'année et comprenez le rôle des basses températures dans l'évolution de l’arbre. Une installation poétique illustrant les quatre saisons vous explique l'endurcissement au gel d’un cerisier. 

#### Défis pour la société

Découvrez le froid "fabriqué", comment il est utilisé dans notre vie quotidienne et dans les secteurs de l’industrie.

Comment utilisait-on le froid avant les machines frigorifiques ? Comment fonctionnent-elles ? Pourquoi le frigo chauffe-t-il votre cuisine ? Pourquoi ne faut-il pas rompre la chaîne du froid ? Quelle est la différence entre congélation et surgélation ? Quelles sont les applications du froid dans les différents secteurs de l’activité humaine et de la vie quotidienne ?

Faire du froid et l'utiliser:  
- Parcourez les trois îlots thématiques de l'espace "Défis pour la société" afin d'en savoir plus sur la quête du froid "fabriqué" et ses applications.
Froid naturel, froid "fabriqué" : suivez l'historique de l’utilisation du froid naturel jusqu’à l’invention des machines frigorifiques et découvrez les phénomènes naturels qui permettent d’abaisser la température.
- Fabriquer et utiliser le froid : obtenez des clés pour comprendre le principe de fonctionnement des machines frigorifiques et les questions concernant leur amélioration dans une problématique de développement durable. Découvrez ou redécouvrez les nombreuses applications du froid dans la vie quotidienne et dans l’industrie.
- Froid et alimentation : découvrez le rôle du froid dans le domaine alimentaire et des techniques comme la surgélation. Observez l’action des basses températures sur les aliments pour mieux comprendre l'effet de vos pratiques quotidiennes : la réfrigération et la congélation. 

###### À ne pas manquer
Range ton frigo ! : rangez des aliments factices dans un frigo : laitages, légumes, viande, œufs, beurre... Obtenez des conseils sur le rangement idéal, sur les gestes écoresponsables et découvrez la température de conservation recommandée pour chaque aliment.

Usine à froid : découvrez comment le froid est utilisé dans les processus de préparation des aliments et les transformations qu’ils subissent grâce au froid avant d’arriver dans notre assiette.

#### Défis pour la science

Dans un décor inspiré des laboratoires des basses températures, familiarisez-vous avec les outils et les travaux des chercheur.euse.s. Découvrez la notion du zéro absolu et l’étrange comportement de la matière à l'approche de cette limite extrême du froid.

Que savons-nous du comportement de la matière à des températures extrêmement basses ? Que se passe-t-il lorsqu’on force des gaz à se liquéfier en les refroidissant ? Pourquoi le fait-on ? Qu’est-ce que le zéro absolu ? Pourquoi ne peut-on jamais l’atteindre ? Comment fait-on pour l’approcher ? Comment se comporte la matière à l’approche du zéro absolu ?

Cryogénie et basses températures:  
Au 19e siècle, on appelait gaz permanents ceux que l’on ne savait pas liquéfier par simple compression à température ambiante : il fallait donc les refroidir. Ce défi scientifique fut vaincu au tournant du siècle. La liquéfaction de ces gaz a permis de générer de très basses températures (inférieures à 120 K ou - 153,15 °C) et de donner naissance à un ensemble de disciplines scientifiques et techniques qui les étudient : la cryogénie.

Les basses températures sont un vrai défi pour ceux qui les manipulent : le froid doit être confiné, maîtrisé et mis en œuvre avec les plus grandes précautions, sous peine de leur échapper !
Dans l'espace "Défis pour la science", explorez les phénomènes liés à l’effet des basses températures sur la matière.

###### À ne pas manquer
Plongées à - 196 °C : plongez virtuellement dans l'azote liquide un ballon gonflé, une fleur, un kiwi, une peluche, du café chaud, un bonbon... Et découvrez ce qu'il se passe !

Des expériences avec du "vrai" froid : l'espace "Défis pour la science" est ouvert sur un espace de médiation où vous pourrez suivre des expériences spectaculaires régulièrement proposées par des médiateur.trice.s scientifiques.

###### Contenus additionnels
- [Approfondir](http://www.cite-sciences.fr/fr/au-programme/expos-temporaires/froid/approfondir/)
- [Site le froid c'est chaud](http://www.cite-sciences.fr/juniors/froid-chaud/)