
// Init clipboard.js
new Clipboard('.email-copy');

$(document).ready(function(){
  // Animate the hint under the email button when copying content
  $('.email-copy').click(function(){
    $(this).addClass('copy-hint').delay(5000).queue(function(next){
      $(this).removeClass('copy-hint');
      next();
    });
  });
});

// Presentation - accordeon, tab for section list
$(document).ready(function(){

  // Accordeon display
  $('.accordeon-toggle').click(function(){
    $(this).parent().toggleClass('opened');
  });

  // Tabs for presentation sections
  $('.section-list-item').click(function(){
		var projectID = $(this).attr('data-id');
    $('.section-list-item').removeClass('active');
    $('.presentation-section').removeClass('active');
    $(this).addClass('active');
    $("#"+projectID).addClass('active');
  });
});

// Call this to switch theme
function switchToDark() {
  $('.m-scene, body').removeClass('light');
  $('.m-scene, body').addClass('dark');

  $('.theme-toggler').removeClass('light-toggler');
  $('.theme-toggler').addClass('dark-toggler');

  $('.theme-indicator.light').removeClass('active');
  $('.theme-indicator.dark').addClass('active');

  $('.logo').removeClass('dark');
  $('.logo').addClass('light');
};
function switchToLight() {
  $('.m-scene, body').removeClass('dark');
  $('.m-scene, body').addClass('light');

  $('.theme-toggler').removeClass('dark-toggler');
  $('.theme-toggler').addClass('light-toggler');

  $('.theme-indicator.dark').removeClass('active');
  $('.theme-indicator.light').addClass('active');

  $('.logo').removeClass('light');
  $('.logo').addClass('dark');
};

// Store in a cookie expiring in one day that a theme has been manualy set
function setLightTheme() {
  Cookies.set('theme', 'light', { expires: 1 });
};
function setDarkTheme() {
  Cookies.set('theme', 'dark', { expires: 1 });
};


$(document).ready(function(){

  // define the time for light theme
  themeTime("07:00:00", "20:00:00");

  function themeTime(start_time, end_time) {
      // get load date
      var dt = new Date();

      // convert both time into timestamp
      var stt = new Date((dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear() + " " + start_time);

      stt = stt.getTime();

      var endt = new Date((dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear() + " " + end_time);

      endt = endt.getTime();

      var time = dt.getTime();

      // Get the stored theme if it exists
      var storedTheme = Cookies.get('theme');

      // Apply theme depending on stored theme or time if theme wasn't stored
      if (storedTheme == 'light') {
        switchToLight();
      } else if (storedTheme == 'dark') {
        switchToDark();
      } else if (time > stt && time < endt) {
        switchToLight();
      } else {
        switchToDark();
      }
  };

  // Change theme and store it in a coockie when clicking the theme button
  $('body').on('click', '.light-toggler', function(){
    switchToDark();
    setDarkTheme();
  });
  $('body').on('click', '.dark-toggler', function(){
    switchToLight();
    setLightTheme();
  });

  var map = {}; // You could also use an array
  onkeydown = onkeyup = function(e){
    e = e || event; // to deal with IE
    map[e.keyCode] = e.type == 'keydown';

    if (map[78] && map[73] && map[75]) {
      $('body').addClass('ohshit');
    }
    // switching theme when pressing the T key
    else if (map[84]) {
      $(".theme-toggler").click();
    }
    // Link to about page when pressing A key
    else if (map[65]) {
      $(".footer-about-btn").click();
    }
    // Going back to landing page on pressing esc key
    else if (map[27]) {
      $(".back-home").click();
    }

    else if (map[82] && map[67]) {
      Cookies.remove('theme');
      console.log('Cookie removed bro! Theme is now based on the time.')
    }
  }
});


$(document).ready(function(){
  // Displaying a project when clicking it's link
  $('.project-item').click(function(){
		var projectID = $(this).attr('data-id');

    // show the hand anim if the project you clicked is already displayed
    if ($("#"+projectID).hasClass('visible')) {
      $("#"+projectID).addClass('project-pointer').delay(1000).queue(function(next){
        $("#"+projectID).removeClass('project-pointer');
        next();
      });
    }

    // Display the project with a delay for animation if another project is already displayed
    else if ($('.project-container').hasClass('visible')) {

      $('html, body').animate( { scrollTop: 0 }, 300 );

      $('.project-container').clearQueue();
      $('.project-container').removeClass('animating').delay(300).queue(function(next){
        $('.project-container').removeClass('visible');
        next();
      });
      $('.project-item').removeClass('active');
      $('.project-container').removeClass('project-pointer')

  		$(this).addClass('active');
  		$("#"+projectID).delay(300).queue(function(next){
        $("#"+projectID).addClass('visible');
        next();
      });
      $("#"+projectID).delay(0.1).queue(function(next){
        $("#"+projectID).addClass('animating');
        next();
      });
    }

    // display the project without delay if nothing is displayed yet
    else {
      $(this).addClass('active');
      $("#"+projectID).addClass('visible');
      $("#"+projectID).delay(0.1).queue(function(next){
        $("#"+projectID).addClass('animating');
        next();
      });
    }
	});
});


// Show the "up to project list button" when you scrolled the height of the project list section
$(window).on('scroll', function(){
  var projectScrollPosition = window.pageYOffset;
  var projectListHeight = $('.project-list-section').outerHeight();

  if (projectScrollPosition > projectListHeight) {
    $('.scroll-to-projectlist').addClass('visible');
  } else {
    $('.scroll-to-projectlist').removeClass('visible');
  };
});

// Scrolling top when clicking the "up to project list button"
$(document).ready(function(){
  $('.scroll-to-projectlist').click(function(){
    $('html, body').animate( { scrollTop: 0 }, 300 );
			return false;
  });
});

// Init smoothstate for page transitions. Animations are done with css _sass/alune-theme/_animations.scss
$(function(){
    'use strict';
    var options = {
        prefetch: true,
        cacheLength: 2,
        onStart: {
          duration: 350, // Duration of our animation
          render: function ($container) {
            // Add your CSS animation reversing class
            $container.addClass('is-exiting');

            // Restart your animation
            smoothState.restartCSSAnimations();
          }
        },

        onReady: {
          duration: 0,
          render: function ($container, $newContent) {
            // Remove your CSS animation reversing class
            $container.removeClass('is-exiting');

            // Inject the new content
            $container.html($newContent);

          }
        },

        onAfter: function($container, $newContent) {

        }
    },
    smoothState = $('#main').smoothState(options).data('smoothState');
});
