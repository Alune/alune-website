---
titre: "🔥 Yule log 🔥"
lang: "en"
subtitre: "loop animation about a log"
draft: false
layout: "sujet"
notes-url: 
numero: 3
description: |-

  - [**Animator's survival kit**](https://mega.nz/#F!3npRgA7T!K4LHfnRf_oiqRqd0lUFwrQ)
  - [**Photoshop animation workflow**](https://www.youtube.com/watch?v=l1-LN2NlB54)  


  Create an animation inspired by the event
  [*Yule Log*](https://en.wikipedia.org/wiki/Yule_Log_(TV_program)) 
  and it's [animated version](https://vimeo.com/yulelog) which happened between 2013 and 2016.
  Votre travail sera régit par quatre contraintes :
  - The video must have a relation to christmas / fireplaces.
  - First and last frame bust be the same so the video loops seamlessly.
  - Some of your animation must be done frame by frame (drawing, stop motion, rotoscoping etc.), you can add secondary elements in any other animation software.
  - The video must be in 9/16 aspect ratio, you can go 1080 wide, 1920 high.
refs: |-
  * [Yule log ofc](https://vimeo.com/yulelog)
  * [Vintage loops](https://www.behance.net/gallery/83211923/Vintage-Loops)
  * [This Must Be Where Pies Go When They Die](https://vimeo.com/275199232)
  * [Loop de Loop challenge](https://vimeo.com/loopdeloop)
rendu: |-
  * A video between 15 and 20 seconds long.
tech: |-
  Animation image par image, utilisation des fichiers en suite d'image, 3D Ae
date-distribution: "2 décembre"
date-rendu: "January 27"
---