---
titre: "Countdown"
lang: "en"
subtitre: "Introduction Ae / Animation"
draft: true
layout: "sujet"
notes-url: 
numero: 1
description: |-
  For this assignement you will have to create an animated countdown from 10 (or more) to 0. The graphic style can go in any direction as long as it stays consistent. You must animate everything appearing on screen appart from live action footage if you use some. Feel free to create this countdown for a specific usecase, designing a coherent visual style to match it. Be care full not to re-use animations multiple times, each step of the countdown must be unique.
refs: |-
  [Not Real TV countdown](https://www.instagram.com/p/CUfnjAPB6gH/?utm_medium=copy_link)  
  [Complete version](https://notreal.tv/ideas-in-progress)
  
rendu: |-
  * A video of 10 seconds or more
tech: |-
  Vitesses d'animations, calques de forme, masks.
date-distribution: "4 novembre"
date-rendu: "October 29th"
---