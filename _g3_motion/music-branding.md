---
titre: "Music Branding"
lang: "en"
subtitre: ""
draft: false
layout: "sujet"
notes-url: 
numero: 2
description: |-
  Create the branding of a music release, consisting of :
  - a generic communication video, **square format**
  - a looping video for the cover, displaying the artist and the name of the release, **square format**
  - a video containing loops showcasing each of the tracks on the release, displaying track names and lengths, there must be transitions / well timed cuts between the tracks. **square format**  

  Your project will be developped arround a consistant visual style, revolving around **animated typography**. You should choose an existing release.    
  All of the videos will have sound from the release.
refs: |-
  - [Aummah covers](https://www.instagram.com/p/CGl-YZCgOTO/)  
  - [Rhox_](https://www.youtube.com/watch?v=QWS7XO2579M)
  - [Apple music animated covers](https://www.youtube.com/watch?v=Wx5A4Uv_Wos)
  - [DIA - A-Track live visuals](https://dia.tv/project/a-trak/)
  
rendu: |-
  * A social media marketing video
  * A looping video cover
  * A video showcasing each tracks
tech: |-
  
date-distribution: 
date-rendu: "December 16th"
---