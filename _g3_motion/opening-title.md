---
titre: "Opening title"
lang: "en"
subtitre: ""
draft: true
layout: "sujet"
notes-url: 
numero: 4
description: |-
  Finishing the school year in motion graphics, you will create the opening title sequence of the movie or TV show of your choice. The only rule is to display the names and titles of everyone as well as the title of the movie or the show. The order in which everything appear is up to you. The goal of this assignment is to create visuals completementing the story and the visual style of the piece, without revealing any crucial informations about it.
refs: |-
  * [Art of the title](https://www.artofthetitle.com/titles/)  
  * [Stranger things s01 title](https://www.artofthetitle.com/title/stranger-things/)
  * [Isle of dogs title](https://www.artofthetitle.com/title/isle-of-dogs/)  
  * [Enter the void title](https://www.artofthetitle.com/title/enter-the-void/)
rendu: |-
  * A title sequence in video format
tech: |-
  
date-distribution: 
date-rendu: "March 18th"
---