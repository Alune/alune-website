---
title: Clojure Paris
role: branding, font-end
ext-url: http://clojure.paris
date: 2017-01-10 18:42:42 +0200
position: 2
image: clp-thumbnail.jpg
project-id: clojure
---
Clojure Paris is community regrouped around the [Clojure](https://clojure.org/){:target="_blank"} programming language. They organize workshops and meetups in Paris. I created their branding and website, around the Clojure syntax and the lambda character, symbole of the language.
