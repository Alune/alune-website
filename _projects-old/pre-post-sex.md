---
title: Pre Post Sex
role: concept, design, front-end
ext-url: http://prepostsex.github.io
date: 2017-01-10 18:42:42 +0200
position: 5
image: pps-thumbnail.jpg
project-id: prepostsex
---
Pre&nbsp;Post&nbsp;Sex is a part of *Our own private Internet*, it's about someone's needs to capture private moments after sharing intimacy with someone else. We designed and developped it over a weekend with [Sixtyne&nbsp;Perez]() and [Morgane&nbsp;Bartoli](http://morganebartoli.com){:target="_blank"}. Our goal was to represent the feeling you can get of never happening and never ending at the same time. Trying to capture our emotions through repeating experiences, good and bad.
