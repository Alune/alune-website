---
title: Please Type Well
role: concept, design, front-end
ext-url: http://pleasetypewell.alune.fr
date: 2017-01-09 18:42:42 +0200
position: 4
image: ptw-thumbnail.jpg
project-id: pleasetypewell
---
Please Type Well is a website about typographic guidelines. It’s creation came from the frustration of seeing basic typographic mistakes in everyday life communication. It was also a chance for me to go through the *Lexique des règles typographiques en usage à l’imprimerie nationale* from top to bottom. Feel free to send me an email if you have any suggestion, if you see a mistake or anything else.
