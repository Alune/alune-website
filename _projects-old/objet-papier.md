---
title: Objet Papier
role: branding, front-end, motion
ext-url: http://objetpapier.fr
date: 2017-01-09 18:42:42 +0200
position: 3
image: op-thumbnail.jpg
project-id: objetpapier
---
Objet Papier is a micro edition label created by [Ronan&nbsp;Deshaies](http://cargocollective.com/ronandeshaies){:target="_blank"} and [Morgane&nbsp;Bartoli](http://morganebartoli.com){:target="_blank"}. The concept is collaborative, so every issue is made by one or more artists, everytime exploring the label's guideline&thinsp;: relations between printed, analogic and numeric mediums. We created the label's visual Identity and website with Morgane, we wanted to find a way to express the relations between these mediums, from emulating crambled paper in vector to actualy scanning photocopies of the issues to display it on the website. All the experimentations were a realy fun part of the process.
