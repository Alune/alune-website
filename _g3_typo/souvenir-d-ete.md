---
titre: "Souvenirs d'été "
lang: "fr"
subtitre: ""
draft: false
layout: "sujet"
notes-url: 
numero: 1
description: |-
  Afin de se faire une idée des aptitutes de chacun à la composition typographique, votre premier projet sera un objet imprimé concernant vos activités de l'été.  
  Deux possibilités :
  - Vous avez effectué un stage cet été, dans ce cas ce projet portera sur la mise en page et la composition typographique de celui-ci. Cette note sera différente de celle que vous aurez avec Guillaume.
  - Vous n'avez pas fait de stage, dans ce cas à vous de choisir ce que vous racontez sur cette période estivale. Quelques contraintes : l'objet imprimé doit être au format A4, il doit comporter du texte et des images (de vous ou non).
refs: |-
  - Librairie Cahier Central
  - Librairie Sans Titre
  - Quintal éditions
  - IGO studio
  - Télévision Magazine
rendu: |-
  - un objet imprimé
tech: |-
date-distribution: ""
date-rendu: "9 novembre"
---