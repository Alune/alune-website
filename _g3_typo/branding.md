---
titre: "Event branding"
lang: "fr"
subtitre: ""
draft: false
layout: "sujet"
notes-url: 
numero: 2
description: |-
  Afin de développer vos aptitudes à la composition typographique, ce sujet porte sur la conception d'une identité visuelle **typographique** dédiée à la communication d'un événement culturel de votre choix.
  Deux types de supports, deux notes :   
  1. **Rendu imprimé :** rendu le 14 décembre, quatre supports imprimés  
    - l'affiche de lévenement, imprimé en A2, contenant un recapitulatif de l'événement, du sujet, des intervenants, du lieu et de la date etc.
    - un objet imprimé distribué au public, contenant les informations utiles (présentation, horaires, intervenants, plans etc.)
    - un ticket d'entrée, recto seul, décliné en deux versions : accès classique, accès premium <a href="/assets/downloads/ticket.ai" download>↓ fichier à télécharger</a>. Format final : 220 x 80mm
    - un bracelet, recto seul, décliné en deux versions : accès classique, accès premium <a href="/assets/downloads/bracelet.ai" download>↓ fichier à télécharger</a>. Format final :  270 x 15mm  
  2. **Rendu numérique :** rendu le 1er février, deux supports numériques sous forme de prototype (figma, XD ou sketch)  
    - une série de maquettes du site internet présentant l'événement, support marketing pour donner envie de participer à l'événement, permettant d'acheter, connaitres le programme, contacter etc. (format desktop 1920 x 1080px)
    - une série de maquettes d'app mobile accessible lors de lévénement avec les infos utiles, support fonctionnel pour les personnes présentes sur l'évenement (format mobile 1080 x 1920px)  

  Si vous avez le temps vous pouvez décliner votre identité sur des goodies, des vidéos etc. Attention dans le cas ou ces propositions prévalent les autres rendus, attendez vous a un retour et une note salé·e de la part de votre chère et tendre.
refs: |-
  - [Zi Sweet](https://the-brandidentity.com/project/platforms-flavoursome-divergent-brand-for-zi-sweet-revels-in-its-classic-contemporary-contrast)
  - [Plateful](https://the-brandidentity.com/project/heydays-and-goods-highlight-platefuls-love-for-the-planet-food-and-people-in-its-sincere-identity)
  - [Tilt](https://the-brandidentity.com/project/gusmansons-combative-rebrand-of-tilt-encapsulates-their-unique-perspective-on-disinformation)
  
rendu: |-
  - quatres rendus imprimés
  - deux rendus numériques
tech: |-
date-distribution: ""
date-rendu: "14 décembre + 1er février"
---