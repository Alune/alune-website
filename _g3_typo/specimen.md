---
titre: "Spécimen typo"
lang: "fr"
subtitre: ""
draft: true
layout: "sujet"
notes-url: 
numero: 2
description: |-
  Lors de la sortie d'une nouvelle police de caractère, il est coutume de réaliser un ou plusieurs supports de communication, visant à promouvoir et mettre en lumière les détails de celle-ci. Ces supports sont appelés des spécimens.  
  Ce sujet porte sur la conception d'un spécimen typographique sous la forme d'un site web avec une ou plusieurs pages. Vous utiliserez la police de caractère de votre choix, comprenant autant de graisses (thin, medium, bold etc.) et de styles (italic, extended, narrow etc.) qu'il vous plaira. Vous développerez un univers graphique en accord avec la typographie, et produirez des visuels afin d'habiller votre spécimen.
  Le site devra afficher :  
  - Le titre de la police
  - Le nom de la fonderie
  - L'année de production
  - La liste des glyphs de votre police de caractère, exhaustive ou non.  
  Mis à part cela, le contenu de votre production est libre, il peut être uniquement constitué d'expérimentations graphiques, comporter du texte (récupéré ou produit) ou être un mix des deux.
refs: |-
  Imprimées :  
    - [La perruque](http://la-perruque.org/)
    - [Alexandrine Thore](http://alexandrine.thore.free.fr/specimensTypo.php)
    - [Morion](https://www.behance.net/gallery/47632005/Morion-Typeface-Specimen)
    - [LuxTypo](https://luxtypo.com/collections/goods)
    - [Ahn Sang Soo and AG Typography Institute](https://letterformarchive.org/news/view/from-the-collection-ahn-sang-soo)
    - [Tim Ruxton](https://www.underconsideration.com/fpo/archives/2013/02/tim-ruxton-self-promo-type-specimen-poster.php)
  Numériques :  
    - [IBM Plex](https://www.ibm.com/plex/)
    - [Micro typeface](https://www.julienlelievre.com/projets/gemeli-micro-type-specimen/)
    - [Scope typeface](https://scope-typeface.com/)
    - [GT Flexa](https://www.gt-flexa.com/)
    - [Faune](http://www.cnap.graphismeenfrance.fr/faune/)
    - [Animography](https://animography.net/collections/typefaces)
    - [Christoph Niemann’s ar cover](https://www.newyorker.com/video/watch/introducing-christoph-niemann-augmented-reality-covers)
    - [DIA](https://www.instagram.com/dia_studio/)
    - [Type terms](https://www.supremo.co.uk/typeterms/)
  Fonderies :  
    - [Grilli type](https://www.grillitype.com/)
    - [Velvetyne](https://velvetyne.fr/)
    - [Production Type](https://www.productiontype.com/)
    - [Use and Modify](https://usemodify.com/)
    - [The Designers Foundry](https://thedesignersfoundry.com/)
  
rendu: |-
  Pré-rendu le 1er décembre, lancement du sujet de charte tv en parallele, rendu final le 8 décembre comprenant :
  - une ou plusieurs pages web (contenant des liens entre elles si multiples)
tech: |-
date-distribution: ""
date-rendu: "8 décembre"
---