---
titre: "Objet éditorial émission TV"
lang: "fr"
subtitre: ""
draft: true
layout: "sujet"
notes-url: 
numero: 3
description: |-
  Afin de décliner l'identité visuelle que vous avez conçu pour l'émission de télévision, vous l'appliquerez sur un objet imprimé. Attention, celui-ci ne fait pas office de charte graphique. Son contenu sera en rapport avec le thème de votre émission et n'a pas à être rédigé par vos soins, vous pouvez récupérer du contenu existant. Articles, photographies et illustrations sont au menu, seule contrainte, vous devez créer des compositions typographiques cohérentes avec votre charte.  
  Le format est entièrement libre, il peut être composé d'un ou plusieurs objets : carnet, livre relié, dépliant, affiche, carte (postale, de visite, routière). Trouvez un ou des supports qui vous intérèssent et qui vous permette de complimenter votre projet vidéo.
refs: |-
  Librairies  
  * [Cahier central](https://cahiercentral.com/)  
  * [Batt Coop](https://battcoop.org/en/)  
  * [Librairie sans titre](https://librairiesanstitre.com/?lang=en)  
  * [Classic Paris](https://www.instagram.com/classic.paris/?hl=en)  
  * [Yvon Lambert](https://www.yvon-lambert.com/)  
  Éditeurs / Projets d'édition  
  * [Rotolux Press](https://www.rotoluxpress.com/en)  
  * [Éditions Neuf Quatre](https://www.editionsneufquatre.com/)  
  * [Garagisme Magasine](https://www.garagisme.com/)  
  * Ill Studio [ADDPMP](https://www.ill-studio.com/projects/addpmp-book) [Incomplete Inventory](https://www.ill-studio.com/projects/incomplete-inventory)  
  * [Audimat](https://audimat-editions.fr/)
  * [Quintal](https://www.quintalatelier.com/)
  
rendu: |-
  Un objet éditorial de votre choix, imprimé. Un PDF sera considéré comme un non rendu.
tech: |-
date-distribution: ""
date-rendu: "9 mars"
---