$(document).ready(function(){
  $('.email-copy').click(function(){
    $(this).addClass('copy-hint').delay(5000).queue(function(next){
      $(this).removeClass('copy-hint');
    next();
    });
  });

  $('.experiment-item').each(function(){
    var expDesc = $(this).children('.exp-info');
    var expLink = $(this).children('.experiment-link');
    var linkWidth = expLink.outerWidth();
    var descWidth = 698 - linkWidth;

    expDesc.css('width', descWidth);

    var descHeight = expDesc.outerHeight();
    var descTopPos = -descHeight/2 + 17;

    expDesc.css('top', descTopPos);

    var expBtn = $(this).children('.exp-info-btn');
    var closeDesc = expDesc.children('.close-desc-btn');
    var closeTopPos = descHeight/2 - 14;

    closeDesc.css('top', closeTopPos);

    var blueBtn = expDesc.children('.link-button');
    var blueBtnWidth = blueBtn.outerWidth()/2;
    var blueBtnMargin = descWidth/2 - blueBtnWidth;

    blueBtn.css('left', blueBtnMargin);

    expLink.click(function(){
      $('.exp-info').removeClass('visible');
      $('.exp-info-btn').removeClass('active');
      $('.experiment-item').removeClass('active');

      expDesc.addClass('visible');
      expBtn.addClass('active');
      $(this).parent('.experiment-item').addClass('active');
    });

    closeDesc.click(function(){
      expDesc.removeClass('visible');
      expBtn.removeClass('active');
      $('.experiment-item').removeClass('active');
    });

  });
});
