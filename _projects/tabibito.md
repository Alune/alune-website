---
layout: project
title: "Tabibito shiatsu"
index: 6
tags:
  - "Branding"
  - "Website design"
  - "Font-end development"
main_image: /assets/images/portfolio/tabibito-main.svg
images: 
excerpt: |-
  Isabelle is a shiatsu parctitioner based in Lorient France. I created a visual identity and website for her practice.
description: |-
  Isabelle is a shiatsu parctitioner based in Lorient France. I created a visual identity and website for her practice.
---


{% include project-img.html src="/assets/images/portfolio/tabibito-1.png" class="img-100 border" %}
{% include project-img.html src="/assets/images/portfolio/tabibito-2.png" class="img-50" %}
{% include project-img.html src="/assets/images/portfolio/tabibito-3.png" class="img-50" %}
{% include project-img.html src="/assets/images/portfolio/tabibito-4.png" class="img-100 border" %}