---
layout: project
title: "Your Agile Way"
index: 1
tags:
  - "Branding"
  - "Interface design"
  - "Font-end development"
main_image: /assets/images/portfolio/your-agile-way-main.svg
images: 
  - value: /assets/images/portfolio/your-agile-way-1.svg
    vertical: false
  - value: /assets/images/portfolio/your-agile-way-4.svg
    vertical: false
  - value: /assets/images/portfolio/your-agile-way-2.svg
    vertical: true
  - value: /assets/images/portfolio/your-agile-way-3.svg
    vertical: true
excerpt: |-
  Your Agile Way is a consulting company accompanying enterprises in their agile transition. I designed their visual identity, design system and developed their website.
description: |-
  Your Agile Way is a consulting company accompanying enterprises in their agile transition. I designed their visual identity, design system and developed their website.
---

{% include project-img.html src="/assets/images/portfolio/your-agile-way-1.png" class="img-100" %}
{% include project-img.html src="/assets/images/portfolio/your-agile-way-4.png" class="img-100" %}
{% include project-img.html src="/assets/images/portfolio/your-agile-way-2.png" class="img-50" %}
{% include project-img.html src="/assets/images/portfolio/your-agile-way-3.png" class="img-50" %}




