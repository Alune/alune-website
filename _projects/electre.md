---
layout: project
title: "Electre"
index: 2
tags:
  - "Branding"
  - "Website design"
  - "Product Design"
  - "User Experience"
  - "Font-end development"
main_image: /assets/images/portfolio/electre-main.svg
images: 
  
excerpt: |-
  Electre Data Service develops software dedicated to the book industry. Im helping them design a new version of their existing application and all the visual elements in order to communicate on it.
description: |-
  Electre Data Service develops software dedicated to the book industry. Im helping them design a new version of their existing application and all the visual elements in order to communicate on it.
---

**Branding and website**  
Starting an overhaul of their product, Electre wanted to modernize their image. Keeping the existing logo and staying close to the historic blue, I defined branding guidelines with a set of complementary colours and the Source as typeface. We decided to create vector illustrations, to bring the project to life.

{% include project-img.html src="/assets/images/portfolio/electre-1.png" class="img-100" %}
{% include project-img.html src="/assets/images/portfolio/electre-2.png" class="img-100" %}
{% include project-img.html src="/assets/images/portfolio/electre-3.png" class="img-50" %}
{% include project-img.html src="/assets/images/portfolio/electre-4.png" class="img-50" %}

**The product**  
The main software offered by the company is a BtoB platform for libraries and bookstore. It contains a rich database, a search engine and various features dedicated to each type of user to help them optimize their workflow.  
The redesign has been a milestone in Electre's development, the previous version being used for more than ten years. We faced very interesting challenges while implementing previous features, improving their user experience and add new ones, all of it trying to create the smallest friction possible for the end user.

{% include project-img.html src="/assets/images/portfolio/electre-ng-1.png" class="img-100" %}
{% include project-img.html src="/assets/images/portfolio/electre-ng-2.png" class="img-100" %}
{% include project-img.html src="/assets/images/portfolio/electre-ng-3.png" class="img-100" %}
{% include project-img.html src="/assets/images/portfolio/electre-ng-4.png" class="img-100" %}