---
layout: project
title: "Print-it"
index: 4
tags:
  - "Publishing"
  - "Website design"
  - "CSS print"
  - "User Experience"
  - "Font-end development"
main_image: /assets/images/portfolio/prm.jpg
images: 
  
excerpt: |-
  Print-it is a project we developed with Objet Papier, an independant publishing house we are four running. The books in this collection are entirely generated with web code and style with CSS print. Alowing us to create hybrid objects between print and digital.
---

**Print-it, let's discover the internet !**  

{% include project-img.html src="/assets/images/portfolio/print-it-1.jpg" class="img-100" %}
{% include project-img.html src="/assets/images/portfolio/print-it-2.jpg" class="img-100" %}
{% include project-img.html src="/assets/images/portfolio/print-it-3.gif" class="img-100" %}
{% include project-img.html src="/assets/images/portfolio/print-it-4.png" class="img-100" %}
{% include project-img.html src="/assets/images/portfolio/print-it-5.jpg" class="img-50" %}
{% include project-img.html src="/assets/images/portfolio/print-it-6b.jpg" class="img-50" %}

**Print-it, surprise !**  

{% include project-img.html src="/assets/images/portfolio/surprise-1.jpg" class="img-100" %}
{% include project-img.html src="/assets/images/portfolio/surprise-2.jpg" class="img-100" %}
{% include project-img.html src="/assets/images/portfolio/surprise-3.jpg" class="img-100" %}
{% include project-img.html src="/assets/images/portfolio/surp4.jpg" class="img-50" %}
{% include project-img.html src="/assets/images/portfolio/surp5.jpg" class="img-50" %}
{% include project-img.html src="/assets/images/portfolio/surprise-6.jpg" class="img-100" %}


<div class="video-wrapper">
  <video autoplay="true" loop="true">
    <source src="/assets/images/portfolio/surprise-lo.mp4" type="video/webm">
  </video>
</div>