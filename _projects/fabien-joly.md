---
layout: project
title: "Fabien Joly"
index: 12
tags:
  - "Branding"
  - "Website design"
  - "Font-end development"
main_image: /assets/images/portfolio/fabien-joly-main.png
images: 
excerpt: |-
  Fabien Joly is a florist and set designer working with luxury brands. I designed and developed his digital visual identity and website.
description: |-
  Fabien Joly is a florist and set designer working with luxury brands. I designed and developed his digital visual identity and website.
---


{% include project-img.html src="/assets/images/portfolio/fabien-joly-1.png" class="img-100 border" %}
{% include project-img.html src="/assets/images/portfolio/fabien-joly-2.png" class="img-100 border" %}
{% include project-img.html src="/assets/images/portfolio/fabien-joly-3.png" class="img-100 border" %}