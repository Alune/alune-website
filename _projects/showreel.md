---
layout: project
title: "Showreel"
index: 5
tags:
  - "Motion graphics"
main_image: /assets/images/portfolio/showreel-main.png
external_url: https://vimeo.com/467368398
images: 
excerpt: |-
  I graduated graphic design with a motion design specialization, it's not my main focus but always a part of the projects I work on. Here you will find my latest showreel, compiling some of my animation work. 
---