---
layout: project
title: "Ytems"
index: 3
tags:
  - "Branding"
  - "Digital product design"
  - "User experience"
  - "User interface"
  - "Font-end development"
main_image: /assets/images/portfolio/ytems-main.svg
images: 
excerpt: |-
  Ytems is an app targeted at independant professionals and their accountant, it allows the automation of accounting tasks for both of them. I am building the product's visual aspect, from branding to ui and implementing all of it in the code.
---


{% include project-img.html src="/assets/images/portfolio/ytems-1.png" class="img-100 border" %}
{% include project-img.html src="/assets/images/portfolio/ytems-2.png" class="img-100 border" %}
{% include project-img.html src="/assets/images/portfolio/ytems-3.png" class="img-50 border" %}
{% include project-img.html src="/assets/images/portfolio/ytems-4.png" class="img-50 border" %}