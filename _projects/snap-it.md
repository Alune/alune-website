---
layout: project
title: "Snap-it mono font"
index: 5
tags:
  - "Type design"
  - "Interface design"
  - "Font-end development"
main_image: /assets/images/portfolio/snap-it-main.svg
external_url: https://objetpapier.fr/snap-it/
images: 
excerpt: |-
  Working on the CSS print project called Print-it with Morgane Bartoli, we designed a monospaced font to use as a title and body copy typeface. It's open source and accessible on my Gitlab page.
description: |-
  Working on the CSS print project called Print-it with Morgane Bartoli, we designed a monospaced font to use as a title and body copy typeface. It's open source and accessible on my Gitlab page.
---