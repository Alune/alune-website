---
layout: project
title: "Gaspard Choron"
index: 7
tags:
  - "Branding"
  - "Print"
  - "Website design"
  - "Font-end development"
main_image: /assets/images/portfolio/gaspard-choron-main.png
images: 
  
excerpt: |-
  Gaspard is a Paris based photographer and chief operator, I made a bold yet low-key identity to match his aesthetic. I implemented it on his website and various digital and print communication items.
---

{% include project-img.html src="/assets/images/portfolio/gaspard-1.png" class="img-100" %}
{% include project-img.html src="/assets/images/portfolio/gaspard-2.jpg" class="img-100 border" %}
{% include project-img.html src="/assets/images/portfolio/gaspard-3.png" class="img-50" %}
{% include project-img.html src="/assets/images/portfolio/gaspard-4.png" class="img-50 border" %}