const body = document.body;

const themeToggler = document.querySelector(".theme-toggler");


function setLight() {
  body.classList.remove("dark");
  body.classList.add("light");
}

function setDark() {
  body.classList.remove("light");
  body.classList.add("dark");
}

function toggleTheme() {
  if (body.classList.contains("dark")) {
    setLight();
    Cookies.set('theme', 'light', { expires: 1, sameSite: 'strict' } );
  } else if (body.classList.contains("light")) {
    setDark();
    Cookies.set('theme', 'dark', { expires: 1, sameSite: 'strict' } );
  }

  const storedTheme = Cookies.get("theme");

  console.log(storedTheme);
}

function delCookie() {
  Cookies.remove('theme')
}

function addAnimation() {
  body.classList.add("animated");
}

document.addEventListener("DOMContentLoaded",function() {
  const storedTheme = Cookies.get("theme");

  if (storedTheme && storedTheme === "dark") {
    body.classList.add("dark");
  } else if (storedTheme && storedTheme === "light") {
    body.classList.add("light");
  } else if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
    setDark();
  } else {
    setLight();
  }
  setTimeout(addAnimation, 10)
})


if (themeToggler) {
  themeToggler.onclick = toggleTheme;
}


const menuButtons = document.querySelectorAll(".menu-button");
const sidebar = document.querySelector(".sidebar");

function toggleMenu() {
  if (body.classList.contains('menu-opened')) {
    body.classList.remove("menu-opened")
    body.classList.remove("lock-scroll")
  } else {
    body.classList.add("menu-opened")
    body.classList.add("lock-scroll")
  }
}

for (let i = 0; i < menuButtons.length; i++) {
  const menuButton = menuButtons[i];
  menuButton.onclick = toggleMenu;
}


// Init clipboard.js
new ClipboardJS('.email-copy');

const email = document.querySelector(".email-copy")
const emailHint = document.querySelector(".email-hint")

function clipboardAnimation() {
  emailHint.classList.add("copy-hint");
  setTimeout(function() {
    emailHint.classList.remove("copy-hint") 
  }, 5000);
}

email.onclick = function() {
  clipboardAnimation()
};


// const body = document.body;

// const themeToggler = document.querySelector(".theme-toggler");

// function setLight() {
//   body.classList.remove("dark");
//   body.classList.add("light");
// }

// function setDark() {
//   body.classList.remove("light");
//   body.classList.add("dark");
// }

// function toggleTheme() {
//   if (body.classList.contains("dark")) {
//     setDark();
//     Cookies.set('theme', 'light', { expires: 1, sameSite: 'strict' } );
//   } else if (body.classList.contains("light")) {
//     setLight();
//     Cookies.set('theme', 'dark', { expires: 1, sameSite: 'strict' })
//   }
// }

// const storedTheme = Cookies.get("theme");


// document.addEventListener("DOMContentLoaded",function() {
//   if (storedTheme && storedTheme === "dark") {
//     setDark()
//   } else if (storedTheme && storedTheme === "light") {
//     setLight()
//   } else if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
//     setDark();
//   } else {
//     setLight();
//   }
//   body.classList.add("animated");
// })

// themeToggler.onclick = toggleTheme();