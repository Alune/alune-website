---
titre: "Emojis"
subtitre: "Introduction Ae / Montage"
draft: false
layout: "sujet"
notes-url: 
numero: 1
description: |-
  Ecrire et mettre en scene une histoire a partir d'éléments graphiques existants : gifs, emojis, memes, archives vidéos etc. À la manière de la video du morceau "Boring Angel" de Oneothrix Point Never, vous n'utiliserez aucun texte ou voix off pour transmettre votre message. Vous devez capitaliser sur les symboles visuels afin d'optenir un résultat singulier. La vidéo devra comporter une bande sonore pouvant être composée de musiques, d'effets sonores, ou les deux. Le format du rendu est imposé a 1920x1080px.
refs: |-
  * Twitch rebranding
  * Boring Angel - Oneothrix point never
rendu: |-
  Une vidéo d'une durée de 30 secondes minimum, une description par écrit de votre projet en quelques lignes.
tech: |-
  Prise en main de l'interface d'after effects, les différents effets, types de calques, les exports via Ae et Media Encoder. 
  Prise en main rapide de Photoshop et Illustrator, dans le but de faire des fichiers utilisables dans Ae. Familiarisation avec les formats, codecs.
  Bases de l'animation dans after effects.
date-distribution: "30 septembre"
date-rendu: "9 novembre"
---