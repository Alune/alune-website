---
titre: "Personnal project"
subtitre: "sujet de fin d'année pour votre projet personnel"
draft: false
layout: "sujet"
notes-url:
numero: 7
description: |-
  Le sujet de votre vidéo est totalement libre pour ce projet, ce qui vous permet de le concevoir autour de votre projet personnel.  
  Quelques contraintes techniques à respecter :  
  - Sera composée entièrement ou en partie d'animation
  - Comprendra de la typographie
  - Durée d'une minute minimum  
refs: |-

rendu: |-
  * Une vidéo d'une minute
  * Un texte de quelques lignes qui decrit votre démarche
tech: |-
  
date-distribution: "7 avril"
date-rendu: "17 mai"
---