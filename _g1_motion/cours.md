---
titre: "Contenu cours"
subtitre: ""
draft: true
layout: "sujet"
notes-url:
numero: 3
description: |-
  icones ? 
  shape layers, cameras
  vector paste as mask ?
  tracking ?
  calques d'effets ?
  Animation typo ?
  3D after ?
  effets de transition ?
refs: |-
  
rendu: |-
  * Une vidéo d'environ 30 secondes 
  * Vos fichiers de travail, depuis un export after effects contenant le projet complet.
tech: |-
  Bases de l'animation et des vitesses dans after effects. Animation des formes prédéfinies dans after effects, masks
date-distribution: "6 novembre"
date-rendu: "27 novembre"
---