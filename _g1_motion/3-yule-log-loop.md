---
titre: "🔥 Yule log 🔥"
subtitre: "loop animation about a log"
draft: false
layout: "sujet"
notes-url: 
numero: 3
description: |-

  - [**Animator's survival kit**](https://mega.nz/#F!3npRgA7T!K4LHfnRf_oiqRqd0lUFwrQ)
  - [**Photoshop animation workflow**](https://www.youtube.com/watch?v=l1-LN2NlB54)  


  Créer une animation inspiré par l'événement 
  [*Yule Log*](https://en.wikipedia.org/wiki/Yule_Log_(TV_program)) 
  et sa [version animée](https://vimeo.com/yulelog) qui à eu lieu entre 2013 et 2015.
  Votre travail sera régit par quatre contraintes :
  - La video doit contenir une buche, comme vous pouvez le constater dans la version animée, vous pouvez jouer sur cette règle de manières variées. 
  - La première et la dernière image doivent être **identiques** (ou se suivre directement) afin d'avoir une lecture en boucle sans discontinuité.
  - Votre vidéo doit contenir des éléments ayant été animés image par image, dans photoshop, sur Ae, sur papier, en stop motion etc. Ce que vous voulez tant que c'est image par image.
  - La vidéo doit être un format portrait en 9:16 full HD, à savoir 1080px par 1920px.
  Une fois les projets terminés ils seront compilés dans une page web cool, qu'on envera tous à la terre entière tellement on sera fières de votre taf ✨  
refs: |-
  * [Yule log ofc](https://vimeo.com/yulelog)
  * [Vintage loops](https://www.behance.net/gallery/83211923/Vintage-Loops)
  * [This Must Be Where Pies Go When They Die](https://vimeo.com/275199232)
  * [Loop de Loop challenge](https://vimeo.com/loopdeloop)
rendu: |-
  * Une vidéo entre 15 et 20 secondes **ni plus, ni moins** afin d'avoir une cohérence dans les rendus.
  * Un texte de quelques lignes qui decrit votre démarche.
tech: |-
  Animation image par image, utilisation des fichiers en suite d'image, 3D Ae
date-distribution: "2 décembre"
date-rendu: "25 janvier"
---