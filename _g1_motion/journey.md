---
titre: "Journey story"
subtitre: "3D environment animation"
draft: true
layout: "sujet"
notes-url:
numero: 4
description: |-
  Vous devez créer une histoire se déroulant dans un environement en 3D. 
  Vous devrez faire avancer l'action grace à des objets et des caméras placées dans l'espace.
  Il s'agit de raconter une histoire et donc de prévoir des actions qui se déroulent tout au long de la vidéo.

  Attention !  
  * L'environement peut être un paysage ou non, à vous de voir, la seul contrainte est que l'on soit dans un environnement en 3D.
  * Il n'y a aucune obligation dans le style graphique, vous pouvez utiliser des illustrations, des photos, du frame by frame, des vidéos etc.
  * Votre projet doit bien évidemment comporter une piste audio.
  * Vous pouvez ajouter ou non du texte, une voix off, ce que vous voulez pour vous aider a raconter votre histoire.
refs: |-
  * [Double King](https://vimeo.com/216131664)
  * [ESPN indent](https://vimeo.com/276082717)
  * [1000 words: Tupac](https://vimeo.com/275730295)
  * [William Hill](https://vimeo.com/159769541)
  * [IBM watson](https://vimeo.com/217528645)
  
rendu: |-
  * Une vidéo entre 40 secondes et 1 minute.
  * Un texte de quelques lignes qui decrit votre démarche.
tech: |-
  3D Ae, caméras, lumières
date-distribution: ""
date-rendu: "9 mars"
---