---
titre: "Timed project ⏱"
subtitre: "Music inspired animation"
draft: false
layout: "timed-subject"
notes-url: 
numero: 4
description: |-  
  **<a href="/assets/downloads/audio-file-timed-project.mp3" download>
  ↓ Fichier mp3 à télécharger
  </a>**  

  Vous avez trois heures pour produire une boucle animée à partir du fichier audio à télécharger ci-dessus, dans un format carré de 1080x1080.  

  Vous devez produire une animation sur la piste audio en question. Celle-ci fait une boucle, libre a vous de faire une animation qui boucle ou non.   

  - Votre vidéo doit durer 12 secondes.
  - La vidéo doit être au format 1080px par 1080px.

  Vous devez rendre le projet After ainsi qu'un export de votre vidéo de moins de 50Mo à la fin de l'examen.
---