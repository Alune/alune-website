---
titre: "Timed project ⏱"
subtitre: "short loop animation"
draft: true
layout: "timed-subject"
notes-url: 
numero: 4
description: |-  
  **<a href="/assets/downloads/evaluation.zip" download>
  ↓ Fichiers Ae à télécharger
  </a>**  

  Vous avez trois heures pour produire une boucle animée à partir du projet After Effects à télécharger ci-dessus.  

  Vous devez partir des formes présentes dans la composition, elles doivent être à cette taille et cette position lors de la première et dernière image de votre vidéo.  
  Vous pouvez changer les couleurs autant que vous voulez, en faire des masques, mettre des videos à l'intérieur etc.
  D'autres formes peuvent bien entendu apparaître dans votre vidéo.  

  - La première et la dernière image doivent être **identiques** afin d'avoir une lecture en boucle sans discontinuité.
  - Votre vidéo doit faire une durée entre 5 et 10 secondes.
  - La vidéo doit concerver le format original de la composition, 1920px par 1080px.
  - Il est préférable d'ajouter du son dans votre vidéo, musique ou effets sonors.

  Vous devez rendre le projet After ainsi qu'un export de votre vidéo de moins de 50Mo à la fin de l'examen.
---