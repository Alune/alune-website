---
titre: "Festival"
subtitre: "Trailer / opening d'un festival"
draft: false
layout: "sujet"
notes-url: ""
numero: 6
description: |-
  Réaliser un trailer / opening pour un festival de motion design réunissant une multitude d'intervenants. Le Festival adopte un thème annuel à définir et une identité visuelle, qui refletera le thème en question. L'identité comprendra un logo animé qui sera present au moins une fois dans  la vidéo. Le nom de chaque intervant doit figurer durant la video ainsi que quelques information sur le déroulé du festival comme le lieu, la date, le thème et le numéro d'édition du festival. La liste des intervenants doit provenir d'une édition de festival existante. Le nom et le concept du festival sont libres.
refs: |-
  * [AICP](https://vimeo.com/341335282)
  * [KIKK](https://vimeo.com/141387499)
  * [FITC](https://vimeo.com/118919656)
  * [Inrocks Festival](https://vimeo.com/184849709)
  * [Motion Motion](https://vimeo.com/265523174)
  * [Motion Motion 2022](https://vimeo.com/690689050)
  * [Develop Denver](https://vimeo.com/229056408)
  * [AMP awards](https://vimeo.com/218830007).
rendu: |-
  * Une vidéo d'une minute minimum.
  * Un texte de quelques lignes qui le thème de cette édition du festival et votre démarche.
tech: |-
  Ae 3d, lumiéres, tracking
date-distribution: "10 Février"
date-rendu: "5 avril"
---