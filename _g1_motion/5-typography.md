---
titre: "Typography"
subtitre: "Clip vidéo centré sur la typographie"
draft: false
layout: "sujet"
notes-url: 
numero: 5
description: |-
  Ce projet consiste à réaliser une vidéo à partir de composition typographiques animées. Le contenu de cette vidéo sera tiré d'une réplique de film (monologue, conversation etc.) Vous pouvez la tirer de film, de dessin animé et de série, MAIS pas d'animé à part Cow Boy bebop. Vous devez concerver la piste audio, vous pouvez y ajouter de la musique si celle-ci n'en contien pas.  
  Votre style graphique et le sens du texte, seront le coeur de votre projet, ces éléments doivent se mettre en valeur, de par leur signification et par leur forme. Attention à vos choix typographiques, pour les inspiration de fontes et associations de fontes > typewolf.com
refs: |-
  Chaine  
  * [Nice Type Channel](https://vimeo.com/channels/nicetype)

  Videos typo  
  * [Spotify behind the lyrics](https://vimeo.com/243970016)
  * [MTV push](https://vimeo.com/318864202)
  * [Apple Don't blink](https://www.youtube.com/watch?v=jk6sz25OZgw)
  * [Bicep - Glue](https://www.youtube.com/watch?v=A7ZxRs45tTg)
  * [Lumen Type](https://vimeo.com/52305654)
  * [Animography](https://animography.net/)
  * [Tom Rosenthal - Don't die curious](https://vimeo.com/256987201)
  * [Lizzo - Good as Hell](https://www.youtube.com/watch?v=WIBwOmCKhE4)

rendu: |-
  * Une vidéo de la durée de votre choix
  * Un texte de quelques lignes qui présente votre projet
tech: |-
  Premiere, Animations de texte, utilisation de caméras, animation sur un tracé, Adobe audition
# date-distribution: "13 janvier"
date-rendu: "8 mars"
---