---
titre: "Geometry"
lang: "fr"
subtitre: "Introduction Ae / Animation"
draft: false
layout: "sujet"
notes-url: 
numero: 2
description: |-
  Production d'une animation à partir de formes géometriques simples. Si les visuels de votre vidéo peuvent être abstraits, leurs signification doit être justifiée. Raconter une histoire, illustrer des idées, vous êtes libre. La présentation de votre projet et de l'idée qu'il véhicule sera le critère principal d'appréciation.
  Le rendu comportera du son, que ce soit de la musique, du sound-design ou une voix-off, vous travaillerez vos animations afin que le son et l'image se magnifient l'un et l'autre. Portez attention à votre direction artistique, votre palette de couleurs et vos vitesses d'animation. Privilégiez la qualité à la quantité.
refs: |-
  * [Ema Colombo life](https://www.instagram.com/p/Ck_RBqaqGrk/) and [Character animation](https://www.instagram.com/p/CLKPQXjpTcG/)
  * [Circles](https://vimeo.com/275431627)
  * [Interruption](https://vimeo.com/257306721)
  * [Ordinary folks](https://vimeo.com/316888093)
  * [Happiness](https://vimeo.com/269817855)
  * [Abstract sticker set](https://vimeo.com/171343267)
  * [Ways to get scared on vimeo](https://vimeo.com/178428779)
##  * IBM [Motion](https://vimeo.com/325231776) and [Colors](https://vimeo.com/325231726)
##  * [Strava year in sport](https://vimeo.com/252266629)
##  * [Meet Bixby](https://vimeo.com/311250597)
##  * [Kavac logo](https://vimeo.com/316184819)
##  * [Google imagery](https://vimeo.com/313004782)
##  * [Wondering](https://vimeo.com/263551281)
##  * [Awwward brand refresh](https://vimeo.com/269014400)
##  * [AWE promo](https://vimeo.com/220093914)
##  * [FITC 2019 - geometric characters](https://www.behance.net/gallery/82996719/FITC-2019-Toronto-Opening-Titles?tracking_source=best_of_behance)
rendu: |-
  * Une vidéo d'environ 30 secondes 
  * Une description de votre projet en quelques lignes
date-distribution: "4 novembre"
date-rendu: "14 decembre"
---