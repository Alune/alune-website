I"w<h1 id="cabanes">Cabanes</h1>
<h5 id="contenu-de-lobjet-éditorial-à-produire">Contenu de l’objet éditorial à produire</h5>

<p>Dans cette exposition, les enfants sont invités à explorer une vingtaine de cabanes taillées à leur mesure. Farfelues ou artistiques, elles renferment des surprises à découvrir et deviennent refuge, cachette, aire de jeux à plusieurs, abri propice au rêve…  Au centre de ces installations, un espace pourvu d’une multitude de matériaux les incite à construire leur cabane. Libre à eux de l’imaginer, de coopérer ou pas avec d’autres enfants, de manipuler et d’assembler toute sorte de matériaux, de fabriquer, de créer…</p>

<h4 id="la-cabane-appliquée-aux-adultes">La cabane appliquée aux adultes</h4>
<p>Patrice Huerre, pédopsychiatre, Saskia Cousin, anthropologue et Fiona Meadows, architecte ont fait partie du comité scientifique de l’exposition Cabanes. Ils nous disent ici en quoi un tel sujet d’exposition est important dans le développement des enfants.</p>

<h5 id="le-point-de-vue-de-patrice-huerre-">Le point de vue de Patrice Huerre :</h5>

<p>Enfants des villes comme enfants des champs vont pouvoir s’en donner à cœur joie. Quelle chance ! Une exposition et non une imposition. Un cadre pensé pour favoriser l’expression libre de chacun et stimuler la curiosité qui n’est pas toujours un vilain défaut…
Des propositions pour déployer son imagination et non des sens obligatoires mis en place par les « grandes personnes ».
Les surprises sont au rendez-vous, tant pour l’enfant se découvrant créer ce qu’il n’avait même pas imaginé, que pour les accompagnants réalisant la richesse créative de leur petit.</p>

<p>En effet, quoi de mieux qu’une cabane pour rêver ? Les enfants, bien sûr, en raffolent. C’est un espace de liberté au milieu des contraintes du quotidien. Mais les adultes en gardent bien souvent la nostalgie. La cabane qui les a occupés dans le jardin, ou à la maison entre deux fauteuils ou sous la table de la salle à manger, est imprimée dans la mémoire, associée aux émotions de l’enfance et à tous les possibles au bout d’un branchage, d’un drap ou de cartons récupérés.</p>

<p>Cabanes à partager, où se rencontrent et se projettent les imaginaires ; cabanes à protéger des intrusions adultes, comme du grand méchant loup, cachées sous les arbres ou dans leurs branches ; cabanes ou s’échangent les secrets qui fondent les fraternités futures ; cabanes où se testent les lois de l’équilibre par lesquelles l’imagination doit passer.</p>

<p>Il est important pour les enfants d’en faire l’expérience afin d’en goûter la saveur inoubliable et le plaisir de créer des univers si près – et si loin en même temps - des bruits de la ville.</p>

<p>Qui n’a pas une cabane dans un coin de sa tête ? Imaginée et espérée ; réalisée puis abandonnée aux caprices des saisons à venir.
Cabane construite un jour d’été pour emmagasiner les rêves. Cabane dont la construction et la fragilité ont scellé des amitiés durables…</p>

<p>Vivre une aventure de cabanes est à la portée de toutes et de tous. Une belle occasion pour les adultes de retrouver leur âme d’enfant, tandis que les plus jeunes rêveront d’un monde plein de créativité et respectueux des autres comme des matériaux qui le constituent, tout en développant leur capacité de jeu. Et souvenons-nous qu’on apprend beaucoup en jouant, car le jeu, c’est sérieux !</p>

<p>Patrice Huerre est pédopsychiatre.
Ancien Chef de service Psychiatrie de l’enfant et de l’adolescent de l’Établissement public de santé Erasme à Antony, et ancien Vice-président de la maison des adolescents du Sud des Hauts de Seine, il est aussi cofondateur du Collège International de l’Adolescence (CILA) et travaille entre autres  sur le jeu et sur les cabanes.
Il est l’auteur de L’enfant et les cabanes, publié dans la revue Enfances &amp; Psy, en  2006.</p>

<h5 id="le-point-de-vue-de-saskia-cousin-et-fiona-meadows">Le point de vue de Saskia Cousin et Fiona Meadows</h5>

<p>Rêvons ! Cabanons ! La cabane entre nostalgie et vitalité</p>

<p>Abri par excellence, l’imaginaire de la cabane est infini comme le sont ses déclinaisons à travers le temps et l’espace. Chai, carrelet, toue cabanée, cabanon de plage, paillotte, ou palombière : la cabane traditionnelle permet de rompre quelques heures avec les contraintes de la vie sociale contemporaine. La hutte, la yourte et le tipi invoquent l’ailleurs, l’exotique, voire l’aube de l’humanité. La cabane perchée ? La robinsonnade ou l’ensauvagement, un retour à la nature le plus souvent contrôlé. La cabane a son envers : la bicoque, la construction de guingois qui révèle la précarité du bâti et celle de ses habitants. À l’inverse de la cabane, douce parenthèse nostalgique, la bicoque insulte la cohésion sociale basée sur le culte de la pierre, du dur, de l’immémorial. Dans la cabane, les adultes projettent leur enfance, leurs nostalgies et leurs craintes.</p>

<p>Pour les enfants, rien de tout cela : la cabane n’est pas un souvenir, c’est un espoir : celui d’imaginer faire et refaire le monde, leur monde, et celui de s’y atteler, en pratique. Branches feuillues ou planche à repasser, draps de bain ou corde à sauter, chaque enfant se saisit de ce qui l’entoure, se confronte au bricolage, s’initie à l’architecture. En construisant sa cabane, chaque enfant s’approprie un territoire et le fait sien, réinvente son histoire personnelle et collective avec la grammaire des objets qui l’entourent, produit sa zone de sécurité, son lieu d’expression.</p>

<p>Chaque nouvelle cabane est un chez soi, et donc, ouvre la possibilité d’une invitation donnée à l’autre – parent, fratrie, amis. L’enfant invente ainsi l’hospitalité. La cabane des enfants est un espace-temps de constitution de l’identité et de l’altérité, un temps et un espace d’initiation à la culture matérielle, et à ses enjeux symboliques. Faire sa cabane, c’est participer à construire sa réalité.</p>

<p>Quelle cabane les enfants-rêvent-ils aujourd’hui ? Une cabane pour leur chambre à coucher ? Une cabane internet ? Une cabane d’astronome ? Quelle forme lui donneront-ils ? Plus qu’un simple rêve, la cabane est une invitation au bricolage, une aventure à construire. La cabane est un patrimoine immatériel de notre humanité : ce qui compte, c’est moins la forme que la fin, et surtout, l’infinie variété des manières de faire, des techniques de construction. Les enfants vont avoir ici l’occasion de s’initier à quelques-unes de ces techniques. La cabane est aussi un support privilégié pour aborder l’architecture avec les enfants.  Elle s’adresse directement à l’imagination de chacun, permet d’interroger le réel. Elle incarne l’art de bâtir, notre patrimoine commun.</p>

<p>Saskia Cousin est anthropologue à l’Institut interdisciplinaire d’anthropologie du contemporain (EHESS/CNRS) et maître de conférences à l’IUT de Tours.</p>

<p>Fiona Meadows est architecte et professeure à la Cité de l’architecture &amp; du patrimoine.</p>

<h4 id="cabanes-dartistes">Cabanes d’artistes</h4>

<p><a href="http://www.cite-sciences.fr/fr/au-programme/expos-temporaires/cabanes/cabanes-dartistes/">Les artistes</a> libre à vous d’intégrer des biographies, explication de la démarche, images etc.</p>
:ET