I"�<h1 id="feu">Feu</h1>
<h5 id="contenu-de-lobjet-éditorial-à-produire">Contenu de l’objet éditorial à produire</h5>

<p>Jouer avec le feu ; déclarer sa flamme ; être tout feu, tout flamme ; mettre sa main au feu ; faire des étincelles ; mettre à feu et à sang…</p>

<p>Si notre langue est truffée d’expressions qui révèlent les multiples facettes du feu et qu’il est si présent dans les récits mythiques de toutes les civilisations, c’est qu’il revêt une importance capitale dans l’histoire de l’humanité. Tantôt ami qui réchauffe, éclaire et nous réunit cordialement autour du foyer, tantôt ennemi qui évoque ravage, destruction et violence, il fascine et effraie tout à la fois.</p>

<p>Depuis sa domestication, il fait partie de notre quotidien. Mais il a fallu attendre le 18e siècle et les avancées de la chimie pour que le mystère soit levé et que les scientifiques comprennent cette réaction chimique appelée combustion.</p>

<p>Cette compréhension du phénomène a donc ouvert la voie au développement industriel basé sur la puissance motrice du feu, générant progrès technique et conséquences néfastes sur l’environnement et la santé.</p>

<p>Pourtant, le feu reste toujours aussi terrifiant et indomptable lorsqu’il échappe à notre contrôle. C’est pourquoi, la recherche se poursuit activement pour pouvoir le prévenir, le combattre et nous en protéger.</p>

<p>Grâce à des installations immersives à grande échelle et une scénographie qui tire parti de la beauté, de la puissance et de la féérie du feu, l’exposition a pour thème la maîtrise du feu et se découpe en trois parties :</p>

<h4 id="apprivoiser-le-feu">Apprivoiser le feu</h4>

<p>Si l’on ignore précisément depuis quand les humains utilisent le feu, on sait, grâce aux plus anciennes traces de foyer retrouvées, qu’il faisait déjà partie de la vie domestique de nos ancêtres, il y a plus de 400 000 ans. Il est probable qu’ils aient commencé à le recueillir et à s’en servir, avant même de savoir l’allumer.</p>

<p>Cette première partie de l’exposition s’intéresse donc à sa domestication, à sa maîtrise empirique.</p>

<p>Le feu sert à cuire et à conserver des aliments par séchage et fumage, à travailler le bois, l’os ou la pierre, à se défendre ou faire la guerre, et à transformer la matière. Outre la fabrication du pain et des poteries, il fait naître de nouveaux matériaux tels le plâtre, les métaux, les colles, le verre, la céramique… L’exposition raconte comment depuis la préhistoire, le feu est synonyme d’une fantastique inventivité technique.</p>

<p>Elle apporte aussi un éclairage sur la production et les procédés d’allumage du feu (percussion,  friction, ou d’autres procédés physico-chimiques plus récents), et montre la grande diversité de ses usages  à travers la présentation de fac-similés d’objets (éclats de silex, extrémité d’épieu, vase en argile cuite, parure de perles en cuivre, etc.).</p>

<p>Employé pour éclairer et chauffer, le feu joue un rôle primordial dans la socialisation et la cohésion du groupe, car il réunit et devient un lieu d’échanges et de transmission. La profusion de mythes que l’on retrouve dans toutes les civilisations atteste son importance et son caractère mystérieux. De nombreux récits racontent comment l’être humain obtient le feu : donné par un animal, une divinité, ou une femme, il peut aussi avoir été volé ou trouvé par hasard.</p>

<p>Symbole majeur de ce qui détruit et régénère, il véhicule des significations variées, servant parfois d’intermédiaire pour communiquer avec des dieux ou des esprits, pour éloigner la mort, favoriser la fertilité, ou purifier le corps en vue de renaître. Il est aussi la marque de l’enfer et du péché.</p>

<h3 id="comprendre-le-feu">Comprendre le feu</h3>

<p>Qu’est-ce que le feu ? Comment naît-il ? Quelle est sa nature ? Pendant longtemps, il demeura un grand mystère et ce n’est qu’au 18e siècle, grâce aux progrès de la chimie, qu’une véritable explication scientifique put être apportée.</p>

<p>Cette deuxième partie de l’exposition aborde la connaissance et la compréhension du feu, sa maîtrise scientifique et  industrielle.</p>

<p>Il s’agit donc ici d’expliquer quelles conditions sont nécessaires pour que s’allume un feu et de comprendre le comportement des flammes. Le feu, que les scientifiques nomment combustion, est une réaction chimique entre deux ingrédients : un matériau (le combustible), l’oxygène de l’air (le comburant), un apport d’énergie servant  de déclencheur.</p>

<p>Toute combustion dégage de l’énergie sous forme de chaleur. Une partie de cette énergie entretient la réaction. Le reste est disponible pour être converti en force motrice ou en électricité. La compréhension de ces phénomènes ouvre alors la voie à la révolution industrielle.</p>

<p>Il y a 250 ans, en Europe, la machine à vapeur transforme la chaleur de la combustion en force motrice, marquant un véritable bond en avant technologique et sociétal. Des premières locomotives à vapeur aux moteurs à réaction, l’exposition montre comment le feu, devenu invisible, est mis en boîte dans les fours, les moteurs et les centrales thermiques et comment notre société thermo-industrielle s’est construite sur l’énergie d’une multitude de combustions.</p>

<p>Mais notre société thermo-industrielle paye aujourd’hui le prix fort de cet usage massif de la combustion dans les transports et l’industrie. Ces derniers, responsables de 71% des émissions de gaz à effet de serre, contribuent au changement climatique et produisent des polluants dangereux pour l’environnement et la santé.</p>

<p>Dès lors, les chercheurs tentent d’améliorer l’efficacité de la combustion en conciliant contraintes environnementales, énergétiques et économiques. L’exposition présente différentes pistes explorées par la recherche pour optimiser la combustion tout en réduisant son impact négatif.</p>

<h4 id="combattre-les-incendies">Combattre les incendies</h4>

<p>Malgré sa mise en boîte par la main de l’homme, le feu, tel un être vivant qui naît, se déplace, mange, grossit et meurt,  reste autonome. Lorsqu’il échappe à tout contrôle, il se transforme en incendie qui ravage tout sur son passage.</p>

<p>Cette troisième partie de l’exposition traite du feu hors de contrôle et des moyens  pour prévenir, s’en protéger et lutter contre les incendies.</p>

<p>Pour mieux combattre un incendie, les chercheurs, pompiers et autres professionnels étudient le comportement des flammes et des fumées. Ils s’intéressent aux modes de propagation des feux urbains et de forêt et à chacune de leurs phases : inflammation, croissance et décroissance.</p>

<p>L’exposition permet de comprendre comment l’eau, principal outil de lutte, parvient à éteindre un feu par refroidissement : en passant de l’état liquide à l’état gazeux ; par étouffement : l’eau isole le combustible de l’oxygène de l’air ; ou par inertage, lorsque la vapeur d’eau remplace l’air.</p>

<p>Elle montre comment la recherche s’appuie sur l’analyse d’incendies passés pour en tirer des leçons et mieux prévenir, s’en  protéger et lutter . Elle donne à voir l’équipement, les outils, les techniques développés dans le domaine de la sécurité et l’ingénierie incendie.</p>

<p>Face au feu destructeur et au danger des fumées qui tuent souvent plus que les flammes, il faut aussi savoir prévenir, contenir et protéger. C’est pourquoi, une part de l’exposition est consacrée à l’explication des gestes et des conduites à tenir en cas d’incendie.</p>

<h6 id="contenus-additionnels">Contenus additionnels</h6>
<ul>
  <li><a href="http://www.cite-sciences.fr/fr/au-programme/expos-temporaires/feu/approfondir/">Approfondir</a></li>
  <li><a href="http://www.cite-sciences.fr/fr/au-programme/expos-temporaires/feu/a-lire/">Lire</a></li>
</ul>
:ET